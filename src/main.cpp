/*
  DHCP-based IP printer

  This sketch uses the DHCP extensions to the Ethernet library
  to get an IP address via DHCP and print the address obtained.
  using an Arduino Wiznet Ethernet shield.

  Circuit:
   Ethernet shield attached to pins 10, 11, 12, 13

  created 12 April 2011
  modified 9 Apr 2012
  by Tom Igoe
  modified 02 Sept 2015
  by Arturo Guadalupi

 */

#include <SPI.h>
#include <Ethernet.h>
#include <Controllino.h>
#include "Mudbus.h"
#define RelayLimitToDeclare 38
#define DigitalLimitToDeclare 14
#define Digital2LimitToDeclare 50
#define Digital3LimitToDeclare 81

unsigned long int x=0, y=0, z=0; //set refresh counter to 0
String readString;
int RelayToDeclare = 22;

int RelayPin = 0;
int AnalogToDeclare = 54;
int AnalogPin = 0;
int DigitalToDeclare = 2;

int DigitalPin = 0;
int Digital2ToDeclare = 42;
int Digital2Pin = 0;
int Digital3ToDeclare = 77;
int Digital3Pin = 0;
// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};
EthernetServer server(80);
Mudbus mb;
String localIP;
String toString(const IPAddress& address);
void ControllinoPinInitialization(int PinToDeclare, int LimitToDeclare);
void setup() {
  mb.R[0] = 0x4352; //Controllino ID name for the meantime
  mb.R[1] = 0x4D45;
  ControllinoPinInitialization(RelayToDeclare, RelayLimitToDeclare);
  ControllinoPinInitialization(DigitalToDeclare, DigitalLimitToDeclare);
  ControllinoPinInitialization(Digital2ToDeclare, Digital2LimitToDeclare);
  ControllinoPinInitialization(Digital3ToDeclare, Digital3LimitToDeclare);
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    } else if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // no point in carrying on, so do nothing forevermore:
    while (true) {
      delay(1);
    }
  }
  // print your local IP address:
  Serial.print("My IP address: ");
  localIP = String() + Ethernet.localIP()[0] + "." + Ethernet.localIP()[1] + "." + Ethernet.localIP()[2] + "." +
                       Ethernet.localIP()[3];
  Serial.println(localIP);
}

void loop() {
  switch (Ethernet.maintain()) {
    case 1:
      //renewed fail
      Serial.println("Error: renewed fail");
      break;

    case 2:
      //renewed success
      Serial.println("Renewed success");
      //print your local IP address:
      Serial.print("My IP address: ");
      localIP = String() + Ethernet.localIP()[0] + "." + Ethernet.localIP()[1] + "." + Ethernet.localIP()[2] + "." +
                           Ethernet.localIP()[3];
      Serial.println(localIP);
      //Serial.println(Ethernet.localIP());
      break;

    case 3:
      //rebind fail
      Serial.println("Error: rebind fail");
      break;

    case 4:
      //rebind success
      Serial.println("Rebind success");
      //print your local IP address:
      Serial.print("My IP address: ");
      localIP = String() + Ethernet.localIP()[0] + "." + Ethernet.localIP()[1] + "." + Ethernet.localIP()[2] + "." +
                           Ethernet.localIP()[3];
      Serial.println(localIP);
      // Serial.println(Ethernet.localIP());
      break;

    default:
      //nothing happened
      break;
  }
  mb.Run();
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
         if (readString.length() < 100) {
          readString += c;
         }
        //check if HTTP request has ended
        if (c == '\n') {
          //check get atring received
          Serial.println(readString);

          //output HTML data header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println();

          //generate data page
          if(readString.indexOf("data") >0) {  //checks for "data" page
            x=x+1; //page upload counter

            client.print("<HTML><HEAD>");
            // if(readString.indexOf("relayOn") > 0)
            // if(readString.indexOf("datarelayOn") > 0) mb.R[16] = 1;
            // if(readString.indexOf("datarelayOff") > 0) mb.R[16] = 0;
            //meta-refresh page every 1 seconds if "datastart" page
            if(readString.indexOf("datastart") >0) client.print("<meta http-equiv='refresh' content='1'>");
            //meta-refresh 0 for fast data
            if(readString.indexOf("datafast") >0)
            client.print("<meta http-equiv='refresh' content='0.1'>");
            client.print("<title>EIG Controllino meta-refresh test</title></head><BODY><br>");
            client.print("page refresh number: ");
            client.print(x); //current refresh count
            client.print("<br><br>");

            //output the value of each analog input pin
            AnalogPin = 0;
            AnalogToDeclare = 54;
            while(AnalogPin < 16)
            {
              client.print("analog input" + (String)AnalogPin + " is: ");
              client.print(analogRead(AnalogToDeclare));
              client.print("<BR>");
              mb.R[AnalogPin+5] = analogRead(AnalogToDeclare);
              AnalogPin++;
              AnalogToDeclare++;
            }
           }

          //generate main page with iframe
          else if(readString.indexOf("relay") > 0)
          {
            y=y+1; //page upload counter

            client.print("<HTML><HEAD>");
            if(readString.indexOf("relayOn") > 0) mb.R[21] = 65535;
            if(readString.indexOf("relayOff") > 0) mb.R[21] = 0;
            client.print("<meta http-equiv='refresh' content='1'>");
            client.print("<title>EIG Controllino meta-refresh test</title></head><BODY><br>");
            client.print("page refresh number: ");
            client.print(y); //current refresh count
            client.print("<br><br>");
            client.print("<HTML><HEAD>");
            RelayPin = 0;
            RelayToDeclare = 22;
            while(RelayToDeclare < 38)
            {
              client.print("relay output" + (String)RelayPin + " is: ");
              client.print(bitRead(mb.R[21], RelayPin));
              client.print("<BR>");
              digitalWrite(RelayToDeclare, bitRead(mb.R[21], RelayPin));
              RelayPin++;
              RelayToDeclare++;
            }
          }
          else if(readString.indexOf("digital") > 0)
          {
            z=z+1; //page upload counter

            client.print("<HTML><HEAD>");
            if(readString.indexOf("digitalOn") > 0)
            {
              mb.R[22] = 4095;
              mb.R[23] = 255;
              mb.R[24] = 15;
            }
            if(readString.indexOf("digitalOff") > 0)
            {
              mb.R[22] = 0;
              mb.R[23] = 0;
              mb.R[24] = 0;
            }
            client.print("<meta http-equiv='refresh' content='1'>");
            client.print("<title>EIG Controllino meta-refresh test</title></head><BODY><br>");
            client.print("page refresh number: ");
            client.print(z); //current refresh count
            client.print("<br><br>");
            client.print("<HTML><HEAD>");
            DigitalPin = 0;
            Digital2Pin = 0;
            Digital3Pin = 0;
            DigitalToDeclare = 2;
            Digital2ToDeclare = 42;
            Digital3ToDeclare = 77;
            while(DigitalToDeclare < 14)
            {
              client.print("digital output" + (String)DigitalPin + " is: ");
              client.print(bitRead(mb.R[22], DigitalPin));
              client.print("<BR>");
              digitalWrite(DigitalToDeclare, bitRead(mb.R[22], DigitalPin));
              DigitalPin++;
              DigitalToDeclare++;
            }
            while(Digital2ToDeclare < 50)
            {
              client.print("digital output" + (String)(DigitalPin + Digital2Pin) + " is: ");
              client.print(bitRead(mb.R[23], Digital2Pin));
              client.print("<BR>");
              digitalWrite(Digital2ToDeclare, bitRead(mb.R[23], Digital2Pin));
              Digital2Pin++;
              Digital2ToDeclare++;
            }
            while(Digital3ToDeclare < 81)
            {
              client.print("digital output" + (String)(DigitalPin + Digital2Pin + Digital3Pin) + " is: ");
              client.print(bitRead(mb.R[24], Digital3Pin));
              client.print("<BR>");
              digitalWrite(Digital3ToDeclare, bitRead(mb.R[24], Digital3Pin));
              Digital3Pin++;
              Digital3ToDeclare++;
            }

          }
          else
          {
            client.print("<HTML><HEAD><TITLE>Controllino frame refresh test</TITLE></HEAD>");
            client.print("EIG's Controllino frame meta refresh test");
            client.print("<BR><BR>Controllino analog input data frame:<BR>");
            client.print("&nbsp;&nbsp;<a href='" + localIP + "/datastart' "); //http://192.168.0.106:80/datastart
            client.print("target='DataBox' title=''yy''>META-REFRESH</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP +"/data' ");
            client.print("target='DataBox' title=''xx''>SINGLE-STOP</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP + "/datafast' target='DataBox' title=''zz''>FAST-DATA</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP + "/relayOn' target='RelayBox' title=''aa''>relayOn</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP + "/relayOff' target='RelayBox' title=''bb''>relayOff</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP + "/digitalOn' target='DigitalBox' title=''aa''>digitalOn</a>");
            client.print("&nbsp;&nbsp;&nbsp;&nbsp;<a href='" + localIP + "/digitalOff' target='DigitalBox' title=''bb''>digitalOff</a><BR>");
            client.print("<form action = \"" + localIP + "/relayOn\" method ='get' target = 'RelayBox'>");
            client.print("<input type=\"submit\" value = 'relayOn' />");
            client.print("</form><BR>");
            client.print("<form action = \"" + localIP + "/relayOff\" method ='get' target = 'RelayBox'>");
            client.print("<input type=\"submit\" value = 'relayOff' />");
            client.print("</form><BR>");
            // client.print("<button href = 'http://192.168.0.107:80/datarelayOn' title=''zz''>RelayOn</button><BR>");
            // client.print("<button onclick=\"location.href='http://192.168.0.107:80/datarelayOn'\" type=\"submit\" formtarget=\"DataBox\">Relay On</button><BR>");
            client.print("<iframe src='" + localIP + "/data' ");
            client.print("width='350' height='360' name='DataBox'>");
            client.print("</iframe>");

            client.print("&nbsp<iframe src='" + localIP + "/relay' ");
            client.print("width='350' height='360' name='RelayBox'>");
            client.print("</iframe>");

            client.print("&nbsp<iframe src='" + localIP + "/digital' ");
            client.print("width='350' height='500' name='DigitalBox'>");
            client.print("</iframe><BR></HTML>");

          }
          delay(1);
          //stopping client
          client.stop();
          //clearing string for next read
          readString="";
        }
      }
    }
  }
}
void ControllinoPinInitialization(int PinToDeclare, int LimitToDeclare)
{
  while(PinToDeclare<LimitToDeclare)
  {
    pinMode(PinToDeclare, OUTPUT);
    PinToDeclare++;
  }
}
